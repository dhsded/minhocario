import React, {useState} from 'react';
import { View,Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';

//tela para inserir os dados do minhocario



export default function App() {
    const[nome, setNome] = useState('');
    const[numero, setNumero] = useState('');
    const[peso, setPeso] = useState ('');
    const[temperatura, setTemperatura] = useState ('');
    const[data, setData] = useState ('');


    
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Minhocarios</Text> 



      <TextInput
      style={styles.input}
      value={nome}
      onChangeText={ (nome) => setNome(nome)}
      placeholder="Nome do colaborador"
      />


      <TextInput
      style={styles.input}
      value={numero}
      onChangeText={ (numero) => setNumero(numero)}
      placeholder="Numero do minhocario"
      />

      <TextInput
      style={styles.input}
      value={peso}
      onChangeText={ (peso) => setPeso(peso)}
      placeholder="Peso em (KG)"
      keyboardType="numeric"
      />

      <TextInput
      style={styles.input}
      value={temperatura}
      onChangeText={ (temperatura) => setTemperatura(temperatura)}
      placeholder="Temperatura em Celsius"
      keyboardType="numeric"
      />


      <TextInput
      style={styles.input}
      value={data}
      onChangeText={ (data) => setData(data)}
      placeholder="Insira a data de cadastro"
      />


      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}> Salvar </Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button2}>
        <Text style={styles.buttonText}> Voltar </Text>
      </TouchableOpacity>



    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#87CEFA'

  },

  title:{
    textAlign:'center',
    marginTop:40,
    fontSize:30
  },

    input:{
      backgroundColor:'#FFF',
      borderRadius:10,
      margin:15,
      padding:10,
      color: '#000000',
      fontSize: 23,
    },
    
   
    button:{
      justifyContent:'center',
      alignItems:'center',
      margin:10,
      backgroundColor:'#228B22',
      padding:10,
    },


    
    button2:{
      justifyContent:'center',
      alignItems:'center',
      margin:10,
      backgroundColor:'#ff0000',
      padding:10,
    },

    buttonText:{
      color:'#FFF',
      fontSize:25,
    }
});

