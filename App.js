import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Home from './Telas/Home';
import Sobre from './Telas/Sobre';

export default function App(){

  const Principal = createStackNavigator();

  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Principal.Screen name="Home" component={Home}/>
          <Principal.Screen name="Sobre" component={Sobre}/>

        </Stack.Navigator>
      </NavigationContainer>
  );
}